# Issue Tracker [![License](http://img.shields.io/:license-mit-blue.svg)](http://doge.mit-license.org) [![Developed By](https://img.shields.io/badge/developed%20with%20♥%20by-Emanuele%20&%20Simon-red.svg)](https://emanuelemazzotta.com/)

A simple issue tracker made with node and react, this is the parent component, containing both repositories.

## Deployment

Todo

## Repositories

Child repositories are located here:
[Web View](https://gitlab.com/simonbreiter/issue-tracker-web-view)
[API](https://gitlab.com/simonbreiter/issue-tracker-api)

## Authors

[Emanuele Mazzotta](mailto:hello@mazzotta.me)

[Simon Breiter](mailto:hello@simonbreiter.com)

## License

[MIT License](LICENSE.md) © Emanuele Mazzotta, Simon Breiter
